# README #

This is a collection of small Python 3 scripts, files, and tutorials that were used in learning the language.


### How do I get set up? ###

Each file is self-contained, can be run stand-alone by simply executing it, eg: <python3 sample_file.py>


### Who do I talk to? ###

* Repo owner or admin - Jeffrey Thigpen
